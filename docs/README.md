<em>Bruno Carmo Nunes - 18/0117548</em>

## Utilização
O programa deve ser executado num sistema Linux ou Mac, pois utiliza system("read").
Caso seja executado em Windows, mudar os **system("read")** para **system("pause")**.
Versão do kernel linux 5.2.17-200.fc30.x86_64 executado;

Tenha o *git*, *make* e o compilador *c++* instalado na sua maquina.

Para clonar:

`git clone https://gitlab.com/brunocmo/ep1.git`

Para compilar o programa:

`make`

Para executar o programa:

`make run`

Para limpar os arquivos .o e bin do programa:

`make clean`


O programa consiste num mercado virtual, onde nele tem 3 modulos de operacao,
venda, estoque e recomendacao.

O modo venda tem o registro no caixa dos produtos que o usuario quer comprar.
Caso o CPF do usuario nao seja encontrado, tem-se a criacao de um novo usuario.

O modo estoque, pode se visualizar os produtos do mercado, todos os produtos estao
ordenados por categoria e nome do produto. Pode-se tambem adicionar produtos e atua-
lizar os produtos do estoque.

O modo recomendacao, mostra a partir do CPF do cliente os produtos recomendados a
partir do produtos mais comprados pelo mesmo cliente.

Obs.: Caso tenha erros no programa, provavelmente a lista *cliente.txt* e *produto.txt*
não foram carregadas.

Bibliotecas utilizadas:

  * iostream
  * vector
  * cstring
  * iomanip
  * algorithm

  Para o formato do arquivo *cliente.txt*:

    * Nome do clientes
    * CPF
    * Se é sócio
    * laço de categorias preferidas
    * $ = delimitador de cliente


  Para o formato do arquivo *produtos.txt*:

    * Nome do produto
    * Categoria do produto
    * Valor do produto (R$)
    * Quantidade do produto
    * $ = delimitador dos produtos
