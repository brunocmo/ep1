#ifndef MAIN_HPP
#define MAIN_HPP

#include <iostream>
#include <vector>
#include <cstring>
#include <iomanip>
#include <algorithm>
#include "excecao.hpp"
#include "categoria.hpp"
#include "produto.hpp"
#include "cliente.hpp"
#include "unistd.h"

const 	Excecao e_cpfInvalido("Erro, CPF não cadastrado!");
const   Excecao e_listaClienteVazia("Erro, lista vazia. Cadastre um cliente");
const 	Excecao e_listaProdutoVazia("Erro, lista vazia. Cadastre um produto");
const 	Excecao e_produtoCadastrado("Erro, produto já está cadastrado");
const 	Excecao e_CPFinva("Erro, CPF não é valido");

bool is_number(const std::string& s);
bool valida(string cat, std::vector<Categoria *> v);
bool busca(string produto, std::vector<Produto *> p);
bool busca_cliente(string cpf, std::vector<Cliente *> c);
Produto * inserir(string produto, std::vector<Produto *> p);
Produto * inserir_categoria(string categoria, std::vector<Produto *> p);
Cliente * inserir_cliente(string cpf, std::vector<Cliente *> c);

bool is_number(const std::string& s)
{
    return !s.empty() && std::find_if(s.begin(),
        s.end(), [](char c) { return !std::isdigit(c); }) == s.end();
}

bool valida(string cat, vector<Categoria *> v){
		int a = v.size();

		if(a==0){
			return false;
		}

		for(int i = 0; i < a; i++){
			if(v[i]->get_categoria() == cat){
				return true;
			}
		}
		return false;

}

bool busca(string produto, std::vector<Produto *> p){
      int a = p.size();

      if(a==0){
        return false; //TODO fazer excecao
      }

      for(int i = 0; i < a; i++){
        if(p[i]->get_nome() == produto) return true;
      }
      return false;

}

bool busca_cliente(string cpf, std::vector<Cliente *> c){
	int a = c.size();
try{
	if(a==0){
		throw(e_listaClienteVazia);
	}

	for(int i = 0; i < a; i++){
		if(c[i]->get_cpf() == cpf) return true;
	}
	return false;
}catch(Excecao & e){
	system("clear");
	cout << "Exceçao capturada: " << e.what() << endl;
	system("read");
	return false;
}

}

Produto * inserir(string produto, std::vector<Produto *> p){

  int a = p.size();

  for(int i = 0; i < a; i++){
    if(p[i]->get_nome() == produto) return p[i];
  }
  return NULL;
}

Produto * inserir_categoria(string categoria, std::vector<Produto *> p){

  int a = p.size();

  for(int i = 0; i < a; i++){
    if(p[i]->get_categoria() == categoria) return p[i];
  }
  return NULL;
}

Cliente * inserir_cliente(string cpf, std::vector<Cliente *> c){

  int a = c.size();

  for(int i = 0; i < a; i++){
    if(c[i]->get_cpf() == cpf) return c[i];
  }
  return NULL;
}

template <typename T1>

T1 getInput(){
    while(true){
    T1 valor;
    cin >> valor;
    if(cin.fail()){
        cin.clear();
        cin.ignore(32767,'\n');
        cout << "Entrada inválida! Insira novamente: " << endl;
    }
    else{
        cin.ignore(32767,'\n');
        return valor;
    }
    }
}



#endif
