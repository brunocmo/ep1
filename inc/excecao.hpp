#ifndef EXCECAO_HPP
#define EXCECAO_HPP
#include <iostream>
#include <exception>

using namespace std;

class Excecao : public exception {
private:
    const char* msg;
    Excecao();
public:
    Excecao(const char * rhs) throw() : msg(rhs) {};
    const char * what() const throw();

};

#endif
